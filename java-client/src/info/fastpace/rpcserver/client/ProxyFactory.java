package info.fastpace.rpcserver.client;

import info.fastpace.rpcserver.Constants.Type;
import info.fastpace.utils.StringUtils;

import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.Socket;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ProxyFactory {
	private static final ThreadLocal<Map<String, String>> context = new ThreadLocal<>();
	static {
		context.set(new HashMap<String, String>());
	}
	
	/**
	 * ThreadLocal context
	 */
	public static Map<String, String> getThreadContext() {
		return context.get();
	}
	
	public static <T> T create(Class<T> clazz, final String ip, final int port) {
		if (!clazz.isInterface()) {
			throw new RuntimeException("Api class is not an interface: " + clazz.getName());
		}
		
		@SuppressWarnings("unchecked")
		T instance = (T) Proxy.newProxyInstance(clazz.getClassLoader(), new Class<?>[]{clazz}, new InvocationHandler() {
			@Override
			public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
				if (args == null) args = new Object[0];
				Class<?>[] parameterTypes = method.getParameterTypes();
				boolean upload = false;
				Gson gson = new GsonBuilder().disableHtmlEscaping().create();
				String json;
				if (parameterTypes.length > 0 && InputStream.class.equals(parameterTypes[0])) {
					upload = true;
					json = gson.toJson(Arrays.copyOfRange(args, 1, args.length));
				} else {
					json = gson.toJson(args);
				}
				Socket socket = new Socket(ip, port);
				try {
					socket.setSoTimeout(30000);
					BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
					// Context
					if (!context.get().isEmpty()) {
						String contextAsString = gson.toJson(context.get());
						bufferedWriter.write(Type.context.name());
						bufferedWriter.write(' ');
						bufferedWriter.write(""+contextAsString.length());
						bufferedWriter.write('\n');
						bufferedWriter.write(contextAsString);
					}
					// RPC
					String rpcAsString = method.getName() + "\n" + json;
					bufferedWriter.write(Type.rpc.name());
					bufferedWriter.write(' ');
					bufferedWriter.write(""+rpcAsString.length());
					bufferedWriter.write('\n');
					bufferedWriter.write(rpcAsString);
					bufferedWriter.flush();
					if (upload) {
						InputStream inputStream = (InputStream)args[0];
						IOUtils.copy(inputStream, socket.getOutputStream());
					}
					socket.shutdownOutput();
					Object res = handleResult(socket, method.getReturnType());
					return res;
				} finally {
					if (!InputStream.class.equals(method.getReturnType())) {
						IOUtils.closeQuietly(socket);
					}
				}
			}
		});
		return instance;
	}

	@SuppressWarnings("unchecked")
	protected static <T> T handleResult(Socket socket, Class<T> returnType) throws Exception {
		String status = StringUtils.readLine(socket.getInputStream());
		if (status.equals("1")) {
			StringWriter stringWriter = new StringWriter();
			IOUtils.copy(socket.getInputStream(), stringWriter, StringUtils.DEFAULT_ENCODING);
			String resString = stringWriter.toString();
			throw new RuntimeException("RPC error: " + resString);
		}
		if (returnType.equals(InputStream.class)) {
			return (T) socket.getInputStream();
		}
		StringWriter stringWriter = new StringWriter();
		IOUtils.copy(socket.getInputStream(), stringWriter);
		String resString = stringWriter.toString();
		T res = new GsonBuilder().disableHtmlEscaping().create().fromJson(resString, returnType);
		return res;
	}
}
