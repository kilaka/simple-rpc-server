package info.fastpace.rpcserver;

public final class Constants {
	private Constants() {}
	
	public enum Type {
		context, rpc, upload;
	}
}
