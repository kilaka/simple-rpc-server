# Simple RPC server #

## What? ##
A very simple server that enables invoking remote methods (procedures) on it, including upload and download.
See how simple it is to invoke it:
```
#!java

new RpcTcpServer<>(4444, Api.class, new ApiImpl()).run();
```
It is based on TCP (not HTTP) and JSON.
The API method params and return values can only be primitives, POJOs and input stream - as you would expect from a simple server that has upload and download functionality.
## Why? ##
1. As an employee, the server world that I've seen doesn't need http - not the endless error codes standard, nor the different subprotocols (PUT/GET/etc.). If you want to invoke a method, just do it - doesn't matter how.
2. As a developer that has a lot of ideas for new products. I'd like to develop ideas fast, without the hassle of the technical layer behind. Create a server and invoke stuff from afar. I couldn't find a simple project to do it.

## Quick start ###
Import the projects into your eclipse workspace. You'll see the server as a library and the demo projects.

### Here's the code for fast assessment ###

**Server startup**
```
#!java

public class Main {
	public static void main(String[] args) throws IOException {
		RpcTcpServer<Api> rpcTcpServer = new RpcTcpServer<>(4444, Api.class, new ApiImpl());
		rpcTcpServer.run();
	}
}

```
**Demo command line client**

```
#!bash

echo -e 'rpc\ntestInt\n[1,2]' | nc 127.0.0.1 4444 ; echo
echo -e 'rpc\ntest\n[x,y]' | nc 127.0.0.1 4444 ; echo
echo -e 'rpc\ntest\n[{a:5},{b:6}]' | nc -v 127.0.0.1 4444 ; echo
echo -e 'rpc\ntestDownload\n[{a:5},{b:6}]' | nc -v 127.0.0.1 4444 ; echo
#2 commands: First to invoke RPC. Second to upload the file.
echo -e 'rpc\ntestUpload\n[{a:5},{b:6}]' | nc -v 127.0.0.1 4444 ; echo
(echo -e 'upload\np5DvQXG53EKdhdk7WAUObw8p7Hm13ZWt\n' ; cat ~/Downloads/test.txt) | nc -v 127.0.0.1 4444 ; echo
```

**Demo API**

```
#!java

public interface Api {

	int testInt(int i1, int i2);
	
	String test(String s1, String s2);

	A test(A a, B b);
	
	String test(ArrayList<A> as);

	InputStream testDownload(A a, B b);

	String testUpload(InputStream inputStream, A a, B b);

	class A {
		private int a;
		public int getA() {
			return a;
		}
		public void setA(int a) {
			this.a = a;
		}
	}

	class B {
		private int b;
		public int getB() {
			return b;
		}
		public void setB(int b) {
			this.b = b;
		}
	}
}
```
**Demo API implementation**

```
#!java

public class ApiImpl implements Api {
	
	public ApiImpl() {}

	// echo -e 'rpc\ntestInt\n[1,2]' | nc 127.0.0.1 4444 ; echo
	@Override
	public int testInt(int i1, int i2) {
		System.out.println("Test was invoked!!!");
		return i1+i2;
	}
	
	// echo -e 'rpc\ntest\n[x,y]' | nc 127.0.0.1 4444 ; echo
	@Override
	public String test(String s1, String s2) {
		System.out.println("Test was invoked!!!");
		return s1+"|||"+s2;
	}

	// echo -e 'rpc\ntest\n[{a:5},{b:6}]' | nc -v 127.0.0.1 4444 ; echo
	@Override
	public A test(A a, B b) {
		System.out.println("Test2 was invoked!!!");
		A a2 = new A();
		a2.setA(a.getA() + b.getB());
		return a2;
	}

	@Override
	public String test(ArrayList<A> as) {
		return Arrays.toString(as.toArray());
	}

	// echo -e 'rpc\ntestDownload\n[{a:5},{b:6}]' | nc -v 127.0.0.1 4444 ; echo
	@Override
	public InputStream testDownload(A a, B b) {
		String fileContent = "This is a text file. Input was: " + a.getA() + "," + b.getB();
		StringReader stringReader = new StringReader(fileContent);
		ReaderInputStream readerInputStream = new ReaderInputStream(stringReader);
		return readerInputStream;
	}
	
	// 2 commands: First to invoke RPC. Second to upload the file.
	// echo -e 'rpc\ntestUpload\n[{a:5},{b:6}]' | nc -v 127.0.0.1 4444 ; echo - returns upload id.
	// (echo -e 'upload\np5DvQXG53EKdhdk7WAUObw8p7Hm13ZWt\n' ; cat ~/Downloads/test.txt) | nc -v 127.0.0.1 4444 ; echo - simulate uploading text file:
	@Override
	public String testUpload(InputStream inputStream, A a, B b) {
		try {
			StringWriter writer = new StringWriter();
			IOUtils.copy(inputStream, writer);
			writer.write("...Input: " + a.getA() + ", " + b.getB());
			writer.flush();
			return writer.toString();
		} catch (IOException e) {
			e.printStackTrace();
			return "Failure";
		}
	}
}

```
**Demo java client**

```
#!java

public class Main {
	public static void main(String[] args) throws Exception {
		Api api = ProxyFactory.create(Api.class, "localhost", 4444);
		
		Api.A a = new Api.A();
		a.setA(2);
		Api.B b = new Api.B();
		b.setB(9);

		String res = api.test("x", "y");
		Assert.assertEquals("x|||y", res);
		
		Api.A aRes = api.test(a, b);
		Assert.assertEquals(11, aRes.getA());

		ArrayList<Api.A> as = new ArrayList<>();
		as.add(a);
		as.add(a);
		as.add(a);
		res = api.test(as);
		Assert.assertEquals("[{a=2.0}, {a=2.0}, {a=2.0}]", res);

		InputStream inputStream = api.testDownload(a, b);
		StringWriter stringWriter = new StringWriter();
		IOUtils.copy(inputStream, stringWriter);
		Assert.assertEquals("This is a text file. Input was: 2,9", stringWriter.toString());
		
		URL url = api.getClass().getClassLoader().getResource("uploadTestFile.txt");
		res = api.testUpload(url.openStream(), a, b);
		Assert.assertEquals("Alik is in the house :)...Input: 2, 9", res);
	}
}

```
## TODO ##
* Validate API methods.
* Simplify upload to be invoked in 1 connection.
* Enable returning more than 1 value. Pair? Map?
* Enable context (session-id, transaction-id. etc.)