package info.fastpace.rpcserver.demo.client;


import info.fastpace.rpcserver.client.ProxyFactory;
import info.fastpace.rpcserver.demo.Api;

import java.io.InputStream;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;


public class Main {

	public static void main(String[] args) throws Exception {
		Api api = ProxyFactory.create(Api.class, "localhost", 4444);
		
		Api.A a = new Api.A();
		a.setA(2);
		Api.B b = new Api.B();
		b.setB(9);

		String res;
		res = api.test("x", "y");
		Assert.assertEquals("x|||y", res);
		
		int sessionId = 1;
		ProxyFactory.getThreadContext().put(Api.SESSION_ID_KEY, "" + sessionId);
		
		int sessionIdRes = api.testSession();
		Assert.assertEquals(sessionId, sessionIdRes);

		Api.A aRes = api.test(a, b);
		Assert.assertEquals(11, aRes.getA());

		ArrayList<Api.A> as = new ArrayList<>();
		as.add(a);
		as.add(a);
		as.add(a);
		res = api.test(as);
		Assert.assertEquals("[{a=2.0}, {a=2.0}, {a=2.0}]", res);

		InputStream inputStream = api.testDownload(a, b);
		StringWriter stringWriter = new StringWriter();
		IOUtils.copy(inputStream, stringWriter);
		Assert.assertEquals("This is a text file. Input was: 2,9", stringWriter.toString());
		
		URL url = api.getClass().getClassLoader().getResource("uploadTestFile.txt");
		res = api.testUpload(url.openStream(), a, b);
		Assert.assertEquals("Upload Content:\nAlik is in the house :)\nInput: 2, 9", res);
	}
}
