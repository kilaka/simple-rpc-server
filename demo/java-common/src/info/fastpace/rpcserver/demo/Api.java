package info.fastpace.rpcserver.demo;

import java.io.InputStream;
import java.util.ArrayList;

public interface Api {
	static String SESSION_ID_KEY = "sessionid";
	
	int testInt(int i1, int i2);

	int testSession();

	String test(String s1, String s2);

	A test(A a, B b);
	
	String test(ArrayList<A> as);

	InputStream testDownload(A a, B b);

	String testUpload(InputStream inputStream, A a, B b);

	class A {
		private int a;
		public int getA() {
			return a;
		}
		public void setA(int a) {
			this.a = a;
		}
	}

	class B {
		private int b;
		public int getB() {
			return b;
		}
		public void setB(int b) {
			this.b = b;
		}
	}

}