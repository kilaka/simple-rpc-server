package info.fastpace.rpcserver.demo;

import info.fastpace.utils.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.ReaderInputStream;

public class ApiImpl implements Api {
	
	private ThreadLocal<Map<String, String>> context = new ThreadLocal<Map<String,String>>(); 
	
	public ApiImpl() {}

	// echo -e 'rpc\ntestInt\n[1,2]' | nc 127.0.0.1 4444 ; echo
	@Override
	public int testInt(int i1, int i2) {
		return i1+i2;
	}

	// echo -e 'context 13\n{sessionid=1}rpc 14\ntestSession\n[]' | nc 127.0.0.1 4444 ; echo
	@Override
	public int testSession() {
		String sessionid = context.get().get(SESSION_ID_KEY);
		return Integer.parseInt(sessionid);
	}

	// echo -e 'rpc 14\ntest\n["x","y"]' | nc 127.0.0.1 4444 ; echo
	@Override
	public String test(String s1, String s2) {
		return s1+"|||"+s2;
	}

	// echo -e 'rpc 22\ntest\n[{"a":2},{"b":9}]' | nc 127.0.0.1 4444 ; echo
	@Override
	public A test(A a, B b) {
		A a2 = new A();
		a2.setA(a.getA() + b.getB());
		return a2;
	}

	// echo -e 'rpc 32\ntest\n[[{"a":2},{"a":2},{"a":2}]]' | nc 127.0.0.1 4444 ; echo
	@Override
	public String test(ArrayList<A> as) {
		return Arrays.toString(as.toArray());
	}

	// echo -e 'rpc 30\ntestDownload\n[{"a":2},{"b":9}]' | nc 127.0.0.1 4444 ; echo
	@Override
	public InputStream testDownload(A a, B b) {
		String fileContent = "This is a text file. Input was: " + a.getA() + "," + b.getB();
		StringReader stringReader = new StringReader(fileContent);
		ReaderInputStream readerInputStream = new ReaderInputStream(stringReader);
		return readerInputStream;
	}
	
	// echo -e 'rpc 28\ntestUpload\n[{"a":2},{"b":9}]My Upload Content' | nc 127.0.0.1 4444 ; echo
	@Override
	public String testUpload(InputStream inputStream, A a, B b) {
		try {
			StringWriter writer = new StringWriter();
			writer.append("Upload Content:\n");
			IOUtils.copy(inputStream, writer, StringUtils.DEFAULT_ENCODING);
			writer.write("\nInput: " + a.getA() + ", " + b.getB());
			writer.flush();
			return writer.toString();
		} catch (IOException e) {
			e.printStackTrace();
			return "Failure";
		}
	}
	
	public void setThreadContext(Map<String, String> context) {
		this.context.set(context);
	}
}
