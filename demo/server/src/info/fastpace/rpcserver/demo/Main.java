package info.fastpace.rpcserver.demo;

import info.fastpace.rpcserver.ContextHandler;
import info.fastpace.rpcserver.RpcTcpServer;

import java.io.IOException;
import java.util.Map;

public class Main {
	public static void main(String[] args) throws IOException {
		final ApiImpl apiImpl = new ApiImpl();
		RpcTcpServer<Api> rpcTcpServer = new RpcTcpServer<>(4444, Api.class, apiImpl);
//		rpcTcpServer.addFilter();
		rpcTcpServer.setThreadContextHandler(new ContextHandler() {
			@Override
			public void handle(Map<String, String> context) {
				apiImpl.setThreadContext(context);
			}
		});
		rpcTcpServer.run();
	}
}
