package info.fastpace.rpcserver;

import java.util.Map;

public interface ContextHandler {
	void handle(Map<String, String> context);
}
