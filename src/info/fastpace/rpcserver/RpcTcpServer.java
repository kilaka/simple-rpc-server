package info.fastpace.rpcserver;

import info.fastpace.rpcserver.Constants.Type;
import info.fastpace.utils.StringUtils;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;


public class RpcTcpServer<I> {
	private static final Logger logger = Logger.getLogger(RpcTcpServer.class.getName());
//	public static final Logger logger = Logger.getAnonymousLogger();
//	public static final Logger logger = Logger.getGlobal();
	static {
//		ConsoleHandler handler = new ConsoleHandler();
        // PUBLISH this level
//		logger.setLevel(Level.ALL);
//        logger.addHandler(handler);
	}
	
	private final ServerSocket serverSocket;
	private final List<Method> rpcs = new ArrayList<>();
	private I api;
	private int timeout = 30*1000;

	private ContextHandler contextHandler;

	public <E extends I> RpcTcpServer(int port, Class<I> apiInterface, E api) throws IOException {
		serverSocket = new ServerSocket(port);
		this.api = api;
		Method[] methods = apiInterface.getMethods();
		for (Method method : methods) {
			rpcs.add(method);
		}
	}
	
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public void run() throws IOException {
		logger.info("*** Server running - waiting for connections - port " + serverSocket.getLocalPort() + " ***");
		while (true) {
			final Socket socket = serverSocket.accept();
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						Object result;
						socket.setSoTimeout(timeout);
//						BufferedInputStream bufferedInputStream = new BufferedInputStream(socket.getInputStream());
//						BufferedInputStream bufferedInputStream = new BufferedInputStream(socket.getInputStream()) {
//							@Override
//							public synchronized int read() throws IOException {
//								int read = super.read();
//								return read;
//							}
//							@Override
//							public synchronized int read(byte[] b, int off, int len) throws IOException {
//								int read = super.read(b, off, len);
//								return read;
//							}
//							@Override
//							public int read(byte[] b) throws IOException {
//								int read = super.read(b);
//								return read;
//							}
//						};
//						InputStreamReader inputStreamReader = new InputStreamReader(socket.getInputStream(), "UTF-8");
//						while (true) {
//							String type = readLine(inputStreamReader);
//							if (type == null) break;
//							System.out.println(type);
//						} 
//						java.io.StringWriter writer = new java.io.StringWriter();
//						IOUtils.copy(inputStreamReader, writer);
//						System.out.println(writer.getBuffer().toString());
//						Scanner scanner = new Scanner(socket.getInputStream(), "UTF-8");
//						String type = scanner.nextLine();
						String type = StringUtils.readLine(socket.getInputStream());
						String prefix = Type.context.name() + ' ';
						if (type.startsWith(prefix)) {
							int contextSizeInBytes = Integer.valueOf(type.substring(prefix.length()));
							String contextAsString = read(socket.getInputStream(), contextSizeInBytes);
							Gson gson = new GsonBuilder().disableHtmlEscaping().create();
							java.lang.reflect.Type reflectType = new TypeToken<Map<String, String>>(){}.getType();
							Map<String, String> context = gson.fromJson(contextAsString, reflectType);
							contextHandler.handle(context);
							type = StringUtils.readLine(socket.getInputStream());
						}
						prefix = Type.rpc.name() + ' ';
						if (!type.startsWith(prefix)) {
							returnResult(socket, new Exception("Missing RPC directive."));
						}
						int rpcSizeInBytes = Integer.valueOf(type.substring(prefix.length()));
						String rpcAsString = read(socket.getInputStream(), rpcSizeInBytes);
						int indexOfNewLine = rpcAsString.indexOf('\n');
						String methodName = rpcAsString.substring(0, indexOfNewLine);
						String methodParamsAsString = rpcAsString.substring(indexOfNewLine+1);
						String[] params = getParams(methodParamsAsString);
						result = tryInvokingMethod(methodName, params, socket.getInputStream());
						returnResult(socket, result);
					} catch (Exception e) {
						logger.log(Level.INFO, "Socket threw exception. Socket: " + socket.toString(), e);
						returnResult(socket, e);
						return;
					} finally {
						IOUtils.closeQuietly(socket);
					}
					logger.info("Socket finished successfuly. Socket: " + socket.toString());
				}
			}).start();
		}
	}

	private String[] getParams(String params) {
		if (params.isEmpty()) return new String[0];
		JsonArray jsonArray = (JsonArray) new JsonParser().parse(params);
		String[] array = new String[jsonArray.size()];
		int i=0;
		for (JsonElement jsonElement : jsonArray) {
			array[i] = jsonElement.toString();
			++i;
		}
		return array;
	}

	private String read(InputStream inputStream, int contextSizeInBytes) throws IOException {
		byte[] inputArray = new byte[contextSizeInBytes];
		IOUtils.read(inputStream, inputArray);
		StringWriter stringWriter = new StringWriter();
		IOUtils.write(inputArray, stringWriter, StringUtils.DEFAULT_ENCODING);
		String params = stringWriter.toString();
		return params;
	}

	public static void returnResult(Socket socket, Exception exception) {
		try {
			BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), StringUtils.DEFAULT_ENCODING));
			bufferedWriter.write("1");
			bufferedWriter.newLine();
			bufferedWriter.write(exception.toString());
			bufferedWriter.flush();
		} catch (IOException e) {
			logger.info("Could not return result. Socket: " + socket.toString() + ". Exception: " + e.toString());
		}
	}
	
	public static void returnResult(Socket socket, Object result) throws IOException {
		BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), StringUtils.DEFAULT_ENCODING));
		bufferedWriter.write("0");
		bufferedWriter.newLine();
		bufferedWriter.flush();
		if (result == null) return;
		if (result instanceof InputStream) { // Download
			InputStream input = (InputStream) result;
			try {
				BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(socket.getOutputStream());
				IOUtils.copy(input, bufferedOutputStream);
				bufferedOutputStream.flush();
			} finally {
				IOUtils.closeQuietly(input);
			}
		}else {
			Gson gson = new GsonBuilder().disableHtmlEscaping().create();
			String json = gson.toJson(result);
			bufferedWriter.write(json);
			bufferedWriter.flush();
		}
	}
	
	private Object tryInvokingMethod(String methodName, String[] params, InputStream inputStream) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		for (Method method : rpcs) {
			if (!method.getName().equals(methodName)) continue;
			Class<?>[] parameterTypes = method.getParameterTypes();
			boolean upload = false;
			if (parameterTypes.length >= 1 && InputStream.class.equals(parameterTypes[0])) {
				upload = true;
				parameterTypes = Arrays.copyOfRange(parameterTypes, 1, parameterTypes.length);
			}
			if (params.length != parameterTypes.length)  {
				continue;
			}
			Object[] paramsConverted = new Object[params.length];
			for (int i = 0; i < parameterTypes.length; i++) {
				Class<?> clazz = parameterTypes[i];
				String param = params[i];
				try {
					Object converted = gson.fromJson(param, clazz);
					paramsConverted[i] = converted;
				} catch (JsonSyntaxException e) {
					paramsConverted = null;
					break;
				}
			}
			if (paramsConverted == null || params.length != parameterTypes.length) {
				continue;
			}
			if (upload) {
				Object[] paramsConvertedTemp = new Object[params.length + 1];
				paramsConvertedTemp[0] = inputStream;
				for (int i = 0; i < paramsConverted.length; i++) {
					paramsConvertedTemp[i+1] = paramsConverted[i];
				}
				paramsConverted = paramsConvertedTemp;
			}
			logger.info("Invoking method: " + method.toString());
			Object result = method.invoke(api, paramsConverted);
			return result;
		}
		throw new NoSuchMethodException("No matching RPC for the following input: " + methodName + "-" + Arrays.toString(params));
	}

	public Object getApi() {
		return api;
	}

	public void setThreadContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}
}
